$(document).ready(function() {
    const gallery = $(".gallery");
    lightbox.init(gallery);
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'alwaysShowNavOnTouchDevices': true,
    });
    $.getJSON("/data/gallery.json", function(data) {

       var container = $(".gallery-container");
       data.forEach(photo => {
            var a = $("<a>", {
                href: photo.src,
                title: photo.alt,
                'data-lightbox': "gallery", 
            });

            var img = $("<img>", {
                src: photo.thumbnail,
                alt: photo.alt,
            });

            a.append(img);
            container.append(a);
        });
    });


});

