$(document).ready(function() {

    var entry = $(".calendarEntry");
    // Add the entries to the calendar, build the thumbnail list 
    $.getJSON("/data/calendar.json", function(data) {
        data.sort((a, b) => {
            return a.Day > b.Day ? 1 : -1;
        });

        var day = data[today - 1];

        var dayID = 'day_' + day['Day'];
        var todayDate = new Date();

        var div = $("<div>", {
        });

        var title = $("<h2>");
        title.append("Advent of Crap Infrastructure - Day "+today);
        div.append(title);
        // show days at or later than today

        if (today <= todayDate.getDate()) {
            var intro = $("<p>");
            intro.append(day["CaptionText"]);
            var main_img = $("<img>", {
                src: day['Main_Image'],
                alt: day['MainImageAlt']
            });
            var second_img = $("<img>", {
                src: day['Second_Image'],
                alt: day['SecondImageAlt'],
                class: "secondImage",
            });
            div.append(intro);
            div.append(main_img);
            div.append(second_img);

        } else {
            var para = $("<p>");
            para.append("no peeking before the actual day");
            div.append(para);
        }
            entry.append(div);
    });

    // Snowflake animation

    // Array to store flakes  
    const flakes = [];

    // Randomly generate newflake
function createSnowflake() {
      let flake = document.createElementNS("http://www.w3.org/2000/svg", "svg");

  let numSides = 6; // Start with hexagon
  let sideLength = 50; // Length of each branch
  
  let points = [];
  
  // Center anchor point
  points.push([250, 250]);  

  for (let i = 0; i < numSides; i++) {

    // Calculate x, y for vertex
    let x = 250 + (sideLength * Math.cos(2 * Math.PI * i / numSides));  
    let y = 250 + (sideLength * Math.sin(2 * Math.PI * i / numSides));
    
    // Add [x,y] point 
    points.push([x, y]);

  }

  // Generate coordinates  
  let pointsString = points.map(p => p.join(",")).join(" ");
  
  // Draw symmetric polygon 
  flake.innerHTML = `<polygon points="${pointsString}" />`;
 flake.style.left = Math.random() * 100 + "%";
  var duration = Math.random() * 3 + 10 + "s";
  flake.style.width = Math.random() * 100 +"px";
flake.style.animation = "fall linear "+duration ;
 
      flake.setAttribute("viewBox", "0 0 500 500");
      flake.setAttribute("class", "snowflake");

      // Append & Save 
      document.body.appendChild(flake);
      flakes.push(flake);

    }
    // Main loop
    setInterval(() => {

      createSnowflake();
      
      if(flakes.length > 50) {
        flakes[0].remove(); 
        flakes.shift(); 
    }
  
    }, 1000);
});
