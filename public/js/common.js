$(document).ready(function() {
    // Add the navigation links to the header
    $.getJSON("/data/nav.json", function(data) {

       var nav = $(".navlist");
       data.forEach(link=> {
            var a = $("<a>", {
                href: link.href,
            });
            a.text(link.title);
            var li = $("<li>");
            li.append(a);
            nav.append(li);
        });
    });

    // Add the recent rides to any page that wants them
    // (do that by adding "<div class="ridelist"></div>)
    $.getJSON("/data/posts.json", function(data) {
        var rides = $(".ridelist");
        data.forEach(link=> {
            var a = $("<a>", {
                href: link.href,
            });
            var img = $("<img>", {
                src: link.thumbnail,
                alt: link.title,
            });
            a.append(img);
            rides.append(a);
    });

});

});

