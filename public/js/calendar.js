$(document).ready(function() {

    let adventEntries = [];
    let thumbnailSize = 256;

    // When the current day is clicked, this reveals the image
    function replaceImage($img, newSrc) {
        // Preload new image
        $("<img>").attr("src", newSrc);

        $img.fadeOut(500, () => {

        // Set new src
        $img.attr("src", newSrc);

        })
        .fadeIn(500);

    } 

    // After the page loads, this rotates each visible day's images 
    function imageRotator() {
        currentIndex = 0;
        images = 3;

        function rotate() {
            // Set new image
            adventEntries.forEach(day=> {
                
                element = $("."+day['dayID']);
                element.attr({src: day['imageSources'][currentIndex]});
            });
            currentIndex++;
            if(currentIndex >= images) {
                currentIndex = 0; 
            }
            setTimeout(rotate, 5000); // 5 second delay
        }
        // Start rotation
        rotate();
    }
        
    var cal = $(".calendar");
    // TODO - we may want to update the clip circle size for different displays
    // var svg = $("<svg>");

    var today = new Date();
    let imgWidth = $(window).width() / thumbnailSize * 100;

    // Add the entries to the calendar, build the thumbnail list 
    $.getJSON("/data/calendar.json", function(data) {
        data.sort((a, b) => {
            return a.Day > b.Day ? 1 : -1;
        });
        data.forEach(day=> {
            var dayID = 'day_' + day['Day'];
            var itemDate = new Date(day['Date']);

            // build a list for later use 
            if ((day['Main_Image'] != null) && (today.getDate() > itemDate.getDate())) {
                adventEntries.push({
                    dayID: dayID,
                    imageSources: [ 
                        day['Main_Image'].replace("advent/", "advent/thumbnails/thumb_"),
                        day['Second_Image'].replace("advent/", "advent/thumbnails/thumb_"),
                        day['Caption_Image'].replace("advent/", "advent/thumbnails/thumb_")
                    ]});
            }

            var div = $("<div>", {
                class: 'day',
                id: dayID,
            });

            // this is the default image
            var src = "/img/advent/wheel_purple.jpg";
            var alt = "a purple bicycle wheel shown if there's not a calendar entry";

            // show days later than today
            if (today.getDate() > itemDate.getDate()) {
                src = day['Main_Image'].replace("advent/", "advent/thumbnails/thumb_");
                alt = day['MainImageAlt'];
            }
            // these are just going to be templated pages
            var a = $("<a>", {
                href: '/adventcalendar/' + day['Day']
            });


            var img = $("<img>", {
                src: src,
                alt: alt,
                class: dayID
            });
            if (today.getDate() == itemDate.getDate()) {

                // make it obvious this is interactive
                function pulseImage(image) {
                    animatePulse();
                    function animatePulse() {

                      $(image).animate({
                        width: "110%",
                        height: "110%"
                      }, 500, function() {
                          
                        $(this).animate({
                          width: "100%",
                          height: "100%"
                        }, 500, animatePulse);

                      });
                   };
                }
                pulseImage(img);
                var opened;
                img.on('click', (event) => {
                    // don't open the link the first time
                    if (!opened) {
                        event.preventDefault();
                        opened = 1;
                    }
                    // stop everything bouncing
                    img.stop(true);

                    replaceImage(img, day['Main_Image'].replace("advent/", "advent/thumbnails/thumb_"));
                    adventEntries.push({
                        dayID: dayID,
                        imageSources: [ 
                        day['Main_Image'].replace("advent/", "advent/thumbnails/thumb_"),
                        day['Second_Image'].replace("advent/", "advent/thumbnails/thumb_"),
                        day['Caption_Image'].replace("advent/", "advent/thumbnails/thumb_")
                    ]});
                });
            };

            var num = $("<div>", {
                class: 'number' });
            var a2 = null;
            if (today.getDate() >= itemDate.getDate()) {

                a2 = $("<a>", {
                    href: '/adventcalendar/' + day['Day']
                });
                a2.append(day['Day']);
                num.append(a2);
            } else {
                num.append(day['Day']);
            }
 
            a.append(img);
            div.append(a);
            div.append(num);
            cal.append(div);
        });
    }).then(data => {
        imageRotator();
        // plain JS to move the window to current day
        var current = document.getElementById("day_"+today.getDate());
        current.scrollIntoView({'block': 'nearest'});
    });
});
