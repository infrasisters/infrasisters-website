# Rebooted InfraSisters website

We are moving our website off its current proprietary platform because they are ratcheting up the pricing and we're not using a lot of its features.

This repository contains the files used to more or less reproduce the website as a static site (e.g. no database backend).

* Pages are HTML (mostly `index.html` in folders that match the old locations, to keep the links from breaking)

* Plain CSS with some responsive elements so it should resize ok on small mobile screens. All the CSS is dumped into `main.css`

* A very small amount of Javascript and JSON for some parts that need updated more regularly (like the image gallery). Javascript files live in `/public/js/` and small datasets that they read in JSON format in `/public/data/`

* Images plus thumbnails, these live in `public/img`. There's a folder for the gallery images and another for the ride posters

* The contact form is externally hosted via a free service and embedded in the contact page. We don't have a separate form for mailing list signups any more.

## How it updates

It's configured to use a service called "Gitlab Pages", every time we make a change here (on the `main` branch) it will republish all the content automatically. This setup is defined in a file called `.gitlab-ci.yml`

## How to update it

If you are a member of the InfraSisters group on Gitlab you can make changes to this website. There are some [Gitlab intro tutorials](https://docs.gitlab.com/ee/tutorials/) for using `git` in the way it was designed to give you a local copy, which you can synchronise back and forth with arcane commands

There is also some decent [web-based editing functionality in Gitlab](https://docs.gitlab.com/ee/user/project/web_ide/) for making changes to e.g. HTML pages directly, it's also possible to use the web interface to upload individual files (like images).

### Workflow for adding a new ride page

The rides are all in a directory called `post`, each in a subdirectory with the ride name, then a single `index.html` file. (It was done this way so we didn't break any links when moving from the old platform. This is what I go through to add a new ride:

* Make a directory inside `public/post` with the name and date of the ride
* Copy `index.html` from one of the previous ride pages, and edit it
* Add any images inside `public/img` - there is a `posters` folder just for ride posters
* Run `python scripts/update_gallery.py` to recreate a thumbnail for each of the posters 
* Add a section to `public/data/posts.json` which has the link to the new page. This ensures it gets shown automatically. Example entry:

```
{   
    "href": "/post/no-excuse-november-2024",
    "title": "#NoExcuse - Our Streets Our Nights, Nov 29th 2024",
    "thumbnail": "/img/posters/thumbnails/thumb_Nov_2024_NoExcuse.jpg",
    "extract": "A ride through the city centre of Edinburgh on Nov 29th 2024"
},
```
Note that having the trailing comma after each entry is really important!
* `git add` all the files I changed, commit and push, inevitably notice typos, make tweaks

## Updating the website crash course!

You will need to create an account on Gitlab, and that account needs added to the infrasisters Owners group. 

* [Edit the front page here](https://gitlab.com/infrasisters/infrasisters-website/-/edit/main/public/index.html)

There's a "Preview changes" tab on that page that will show you the changes before you save them,

* [Test version of the front page that you're free to experiment with](https://gitlab.com/infrasisters/infrasisters-website/-/edit/main/public/_index_test.html) - if you're worried about breaking the front page then you can experiment here. This _shouldn't_ end up in search results :)

When you're done, hit "Commit changes" in the top right. It will ask you to leave a polite message about what changes you've made. Once you save the file, the website updates automatically (it might take a few minutes).

The pages are written in a language called HTML. It uses _tags_ in pointy brackets to change the appearance of text. You can get going with two or three basic tags. See that they "open" and "close" - remember to close them!

### Paragraphs

`<p>` starts a paragraph and `</p>` ends a paragraph.

```
<p>Here is a paragraph</p>
```

### Links 

Add a link to another web page with `<a>`. This works a bit differently - the `<a>` creates the link. The `href` is the address of the page that you want to link to. Then any text up to `</a>` is what's shown on the page as the clickable link.

```
<a href="https://infrasisters.org.uk/">InfraSisters</a>
```

### Headings

The tag `<h>` followed by a number gives you a heading. `<h1>` is the biggest heading. `<h4>` is about the size of normal paragraph text. It goes down to `<h6>`, no-one knows why.

```
<h1>Big Bold Header</h1>
```

```
<h3>Smaller sub-header</h3>
```

That is basically all the HTML you really need to know.

## How to test it

You can test it locally by running a tiny webserver, for me the simplest way is to do this with python

```
cd public 
python -m http.server
```

And visit localhost:8000 - depending on your editing software we can find an extension that does the same thing

# TODO

* Find and add images and map links to the historic ride pages (they're not all as-was
* Make sure the `<title>` tags are meaningful and add any `<meta>` tags that might help with SEO
* Make the `.ridelist` display even nicer (preview images, extract sentences?)
* Move the video to peertube.bike - they seem to have gone offline though





 
