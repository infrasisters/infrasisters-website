"""Run this to create thumbnail versions of images and add them to the JSON index"""
from PIL import Image
import json
import os
from pathlib import Path

# Directory containing images
input_directory = 'public/img/posters'
#input_directory = 'public/img/advent'

# Output directory for thumbnails
output_directory = 'public/img/posters/thumbnails/'
#output_directory = 'public/img/advent/thumbnails/'

# Desired thumbnail size
# thumbnail_size = (512, 512)
thumbnail_size = (256, 256)

# JSON file containing image information
json_file_path = 'public/data/gallery.json'

def crop_center(pil_img, crop_width, crop_height):
    img_width, img_height = pil_img.size
    return pil_img.crop(((img_width - crop_width) // 2,
                         (img_height - crop_height) // 2,
                         (img_width + crop_width) // 2,
                         (img_height + crop_height) // 2))

def create_thumbnail(image_path, output_path):
    if os.path.exists(output_path):
        return
    try:
        with Image.open(image_path) as img:

            crop_max = min(img.height, img.width)
            img = crop_center(img, crop_max, crop_max)
            thumb = img.resize(thumbnail_size)

            thumb.save(output_path) 

    except IOError:
        print(f"Cannot create thumbnail for {image_path}")

def create_thumbnails_for_directory(input_dir, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for filename in os.listdir(input_dir):
        if filename.endswith('.jpg') or filename.endswith('.png') or filename.endswith('.jpeg'):
            image_path = os.path.join(input_dir, filename)
            output_path = os.path.join(output_dir, f"thumb_{filename}")
            create_thumbnail(image_path, output_path)
            print(f"Thumbnail created for {filename}")

def update_json_with_new_images(json_file, images_directory):
    with open(json_file, 'r') as f:
        data = json.load(f)

    existing_images = [entry['src'] for entry in data]

    for filename in os.listdir(images_directory):
        if filename.endswith('.jpg') or filename.endswith('.png') or filename.endswith('.jpeg'):
            image_path = Path(images_directory).relative_to('public') / filename
            image_path = f"/{image_path}"
            if str(image_path) not in existing_images:
                # Add information about the new image to the JSON file
                new_entry = {
                    "src": str(image_path),
                    "thumbnail": f"{str(Path(image_path).parent)}/thumbnails/thumb_{filename}",
                    "alt": "Your alt text here"
                }
                data.append(new_entry)
                print(f"Added {filename} to the JSON file")

    with open(json_file, 'w') as f:
        json.dump(data, f, indent=4)

# Create thumbnails for images in the input directory
create_thumbnails_for_directory(input_directory, output_directory)

# Update JSON file with information about new images
#update_json_with_new_images(json_file_path, input_directory)

