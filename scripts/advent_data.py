"""Convert the advent calendar CSV, repeatbly"""
import csv
import json
import requests
from pathlib import Path

del_fields = ["title", "Updated Date", "Created Date", "AdventCalendar (Day)", "ID", "Owner", "Contributor"]
image_fields  = ["Main_Image", "Second_Image", "Caption_Image"]

# Function to convert CSV to JSON
def csv_to_json(csv_file, json_file):
    
    # Open CSV file
    with open(csv_file, encoding='utf-8-sig') as csvf:  
       
        # Load CSV file data using csv library's dictionary reader
        csv_reader = csv.DictReader(csvf)  

        # Convert each CSV row into a JSON object
        json_rows = []
        for row in csv_reader:
            day = row["Day"]
            # Modify/delete specific fields
            # Use a file naming convention for the images
            # We can grab them from Wix and save en route, handy
            for field in image_fields:
                new_path = grab_image(day, field, row[field])

                row[field] = new_path

            # Delete the admin cruft
            for field in del_fields:
                del row[field]

            # Cast types
            row['Day'] = int(row['Day'])
            # Get the geodata
            if row["Location"]:
                print(row["Location"])
                location = json.loads(row["Location"])
                row["Location_Name"] = location['formatted']
                row["geom"] = (location['location']['longitude'], location['location']['latitude'])
                del row["Location"]

            json_rows.append(row)
  
    # Open JSON file
    with open(json_file, 'w', encoding='utf-8') as jsonf: 
        jsonf.write(json.dumps(json_rows, indent=4))
          
def grab_image(day, name, location):
    if not location:
        print(f"No image for day {day}, skipping")
        return
    suffix = name.split('_')[0].lower()
    
    # Get just the image filename
    url = location.replace('wix:image://v1/', '')
    url = url.split('/')[0]

    # Get image file name from URL 
    filename = f'/img/advent/{day}_{suffix}'

    # Detect format and set appropriate file extension
    # Wix doesn't behave correctly so we need to kludge it :/
    # if response.headers["Content-Type"] == "image/jpg":
    print(url)
    if 'jpg' in url or 'jpeg' in url:
        filename += ".jpg"
    #elif response.headers["Content-Type"] == "image/png":
    elif 'png' in url:
        filename += ".png"
    else:
        raise Exception("Unknown file type")

    out_path = f"public/{filename}"
    if Path(out_path).exists():
        return filename

    response = requests.get(f"https://static.wixstatic.com/media/{url}") 

    # Write image to disk   
    with open(out_path, "wb") as out_file:
        out_file.write(response.content)
    return filename 


# Run conversion on sample CSV  
csv_file = 'advent.csv'
json_file = 'public/data/calendar.json'
csv_to_json(csv_file, json_file)

