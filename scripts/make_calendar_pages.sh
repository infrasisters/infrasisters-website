#!/bin/bash

# Base folder 
BASE_FOLDER="../public/adventcalendar"  

# Page template 
TEMPLATE="templates/calendar.html"  

# Generate folders
for i in {1..24}; do
    DAY_DIR="$BASE_FOLDER/$i"
    mkdir -p "$DAY_DIR"
    echo "Created $DAY_DIR"
done

# Copy and process template 
for i in {1..24}; do
    DAY_DIR="$BASE_FOLDER/$i"
    PAGE="$DAY_DIR/index.html" 
    
    cp $TEMPLATE $PAGE
    
    sed -i "s/DAY/$i/g" $PAGE 
    
    echo "Generated $PAGE"
done
